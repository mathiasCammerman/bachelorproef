\select@language {dutch}
\contentsline {chapter}{\numberline {1}Inleiding}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Probleemstelling en Onderzoeksvragen}{6}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Plan van aanpak}{6}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Doel van het onderzoek}{7}{subsection.1.1.2}
\contentsline {chapter}{\numberline {2}Methodologie}{8}{chapter.2}
\contentsline {chapter}{\numberline {3}React Native}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Installatie van de benodigde software}{13}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Installatie op Mac}{13}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Installatie op Windows}{14}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Installatie op Linux}{14}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Vereiste basiskennis}{15}{section.3.2}
\contentsline {chapter}{\numberline {4}Native}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}Installatie van de benodigde software}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}Vereiste basiskennis}{17}{section.4.2}
\contentsline {chapter}{\numberline {5}Vergelijking van de UI-componenten}{18}{chapter.5}
\contentsline {section}{\numberline {5.1}DrawerLayout}{20}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}DrawerLayout bij React Native Android}{21}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}DrawerLayout bij Native Android}{23}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}ViewPager}{26}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}ViewPager bij React Native Android}{27}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}ViewPager bij Native Android}{30}{subsection.5.2.2}
\contentsline {chapter}{\numberline {6}Proof of concept}{33}{chapter.6}
\contentsline {section}{\numberline {6.1}Beschrijving van het Applicatieprofiel}{34}{section.6.1}
\contentsline {section}{\numberline {6.2}Benchmarking}{35}{section.6.2}
\contentsline {chapter}{\numberline {7}Resultaten}{36}{chapter.7}
\contentsline {section}{\numberline {7.1}Vergelijking van de UI-componenten}{36}{section.7.1}
\contentsline {section}{\numberline {7.2}Proof of concept}{37}{section.7.2}
\contentsline {chapter}{\numberline {8}Conclusie}{39}{chapter.8}
\contentsline {chapter}{\numberline {9}Vooruitblik op de toekomst}{40}{chapter.9}
